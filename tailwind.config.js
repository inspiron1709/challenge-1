module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: ['./src/**/*.tsx'],
  theme: {},
  variants: {},
  plugins: [require('@tailwindcss/ui')],
};
