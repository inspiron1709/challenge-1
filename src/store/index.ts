import { combineReducers, createStore, applyMiddleware } from 'redux';
import { all, fork } from 'redux-saga/effects';
import { createWrapper } from 'next-redux-wrapper';
import createSagaMiddleware from 'redux-saga';

import { PictureState } from './picture/state';
import { pictureReducer } from './picture/reducer';
import { pcitureSaga } from './picture/saga';

export interface ApplicationState {
  readonly picture: PictureState;
}

const IsServer = typeof window === 'undefined';

const rootReducer = combineReducers<ApplicationState>({
  picture: pictureReducer,
});

function* rootSaga() {
  yield all([fork(pcitureSaga)]);
}

export const initStore = (initialState = {}) => {
  // create the redux-saga middleware
  const sagaMiddleware = createSagaMiddleware();

  const middlewares = [sagaMiddleware];
  // We'll create our store with the combined reducers/sagas, and the initial Redux state that
  // we'll be passing from our entry point.
  const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares));

  // Don't forget to run the root saga, and return the store object.
  if (!IsServer) {
    sagaMiddleware.run(rootSaga);
  }

  return store;
};

export const wrapper = createWrapper(initStore);
