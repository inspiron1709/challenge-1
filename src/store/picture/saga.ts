import { call, put, all, fork, takeEvery } from 'redux-saga/effects';

import { getPicturesAsync } from './actions';
import { getFlickrPictures } from '~/services/picture.service';

function* handleGetPictures(action: ReturnType<typeof getPicturesAsync.request>) {
  try {
    const pictures = yield call(getFlickrPictures, action.payload);
    yield put(getPicturesAsync.success(pictures));
  } catch (err) {
    console.log(err);
  }
}

function* watchGetPicturesRequest() {
  yield takeEvery(getPicturesAsync.request, handleGetPictures);
}

export function* pcitureSaga() {
  yield all([fork(watchGetPicturesRequest)]);
}
