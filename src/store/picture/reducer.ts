import { createReducer, ActionType } from 'typesafe-actions';

import { getPicturesAsync } from './actions';
import { PictureState } from './state';

type Action = ActionType<typeof getPicturesAsync>;

export const initialPageState: PictureState = {
  loading: false,
  items: [],
};

export const pictureReducer = createReducer<PictureState, Action>(initialPageState)
  .handleAction([getPicturesAsync.request], (state) => ({
    ...state,
    loading: true,
  }))
  .handleAction([getPicturesAsync.success], (state, action) => ({
    ...state,
    loading: false,
    items: action.payload,
  }))
  .handleAction([getPicturesAsync.failure], (state) => ({
    ...state,
    loading: false,
  }));
