import { createAsyncAction } from 'typesafe-actions';

import * as pictureTypes from './type';
import { Picture } from './state';

export const getPicturesAsync = createAsyncAction(
  pictureTypes.GET_PICTURES_REQUEST,
  pictureTypes.GET_PICTURES_SUCCESS,
  pictureTypes.GET_PICTURES_FAILURE,
)<string, Picture[], undefined>();
