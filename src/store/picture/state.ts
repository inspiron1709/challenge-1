export interface Picture {
  title: string;
  link: string;
  author: string;
  tags: string;
  media: {
    m: string;
  };
}

export interface PictureState {
  readonly loading: boolean;
  readonly items: Picture[];
}
