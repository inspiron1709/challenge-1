import React from 'react';

import Header from '../Header';

interface MainLayoutProps {
  children?: JSX.Element[] | JSX.Element | null;
}

const MainLayout = (props: MainLayoutProps) => {
  const { children } = props;
  return (
    <>
      <Header />
      {children}
    </>
  );
};

export default MainLayout;
