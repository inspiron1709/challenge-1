import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';

import { getPicturesAsync } from '~/store/picture/actions';

interface HeaderProps {
  getPictures: typeof getPicturesAsync.request;
}

const Header = (props: HeaderProps) => {
  const { getPictures } = props;
  const [term, setTerm] = useState('');

  useEffect(() => {
    getPictures(term);
  }, [term]);

  return (
    <div className="relative bg-white">
      <div className="px-4 mx-auto max-w-7xl sm:px-6">
        <div className="flex items-center justify-between py-6 border-b-2 border-gray-100 md:justify-start md:space-x-10">
          <div className="lg:w-0 lg:flex-1">
            <Link href="/" passHref>
              <a className="flex">
                <img
                  className="w-auto h-8 sm:h-10"
                  src="https://tailwindui.com/img/logos/workflow-mark-on-white.svg"
                  alt="Workflow"
                />
              </a>
            </Link>
          </div>
          <nav className="flex">
            <div className="relative">
              <div className="relative mt-1 rounded-md shadow-sm md:w-96">
                <div className="absolute inset-y-0 left-0 flex items-center px-3 pointer-events-none">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    className="w-5 h-5 text-gray-400 transition-colors duration-150 group-focus-within:text-gray-500"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                </div>
                <input
                  id="search"
                  className="block w-full pl-10 pr-12 form-input sm:text-sm sm:leading-5"
                  placeholder="Search Flickr Photos"
                  defaultValue={term}
                  onKeyUp={(e) => {
                    if (e.key === 'Enter') {
                      setTerm(e.currentTarget.value);
                    }
                  }}
                />
              </div>
            </div>
          </nav>
          <div className="items-center justify-end hidden space-x-8 md:flex md:flex-1 lg:w-0">
            <a
              href="#"
              className="text-base font-medium leading-6 text-gray-500 whitespace-no-wrap hover:text-gray-900 focus:outline-none focus:text-gray-900"
            >
              Sign in
            </a>
            <span className="inline-flex rounded-md shadow-sm">
              <a
                href="#"
                className="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700"
              >
                Sign up
              </a>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  getPictures: getPicturesAsync.request,
};

export default connect(null, mapDispatchToProps)(Header);
