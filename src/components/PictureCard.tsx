import React from 'react';
import classNames from 'classnames';
import { v4 as uuidv4 } from 'uuid';

import { Picture } from '~/store/picture/state';

interface PictureCardProps {
  content: Picture;
  last: boolean;
}

const PictureCard = (props: PictureCardProps) => {
  const { content, last } = props;

  return (
    <div className="w-full" key={uuidv4()}>
      <div
        className={classNames('flex items-center h-full p-4', {
          'border-b border-gray-200': !last,
        })}
      >
        <a href={content.link} target="_blank" rel="noreferrer" className="flex-shrink-0 mr-4 ">
          <img
            alt="team"
            className="object-cover object-center w-16 h-16 bg-gray-100 rounded-full"
            src={content.media.m}
          />
        </a>
        <div className="flex-grow">
          <h2 className="font-medium text-blue-600 title-font">
            <a href={content.link} target="_blank" rel="noreferrer">
              {content.title}
            </a>
          </h2>
          <p className="text-gray-400">{content.author}</p>
          <p className="text-gray-500">{content.tags}</p>
        </div>
      </div>
    </div>
  );
};

export default PictureCard;
