import fetchJsonp from 'fetch-jsonp';

import { Picture } from '~/store/picture/state';

export const getFlickrPictures = async (term: string) => {
  const response = await fetchJsonp(
    `https://api.flickr.com/services/feeds/photos_public.gne?tags=${term}&format=json`,
    {
      jsonpCallbackFunction: 'jsonFlickrFeed',
    },
  );

  const data = await response.json();
  return data.items as Picture[];
};
