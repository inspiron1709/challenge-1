import React from 'react';
import { connect } from 'react-redux';

import { MainLayout } from '@/components';
import { Picture } from '@/store/picture/state';
import { ApplicationState } from '../store/index';
import PictureCard from '~/components/PictureCard';
import { v4 as uuidv4 } from 'uuid';

interface HomePageProps {
  pictures: Picture[];
}

const HomePage = (props: HomePageProps) => {
  const { pictures } = props;
  return (
    <MainLayout>
      {pictures.length > 1 ? (
        <div className="container px-5 mx-auto">
          <div className="flex">
            <div className="w-full p-2">
              {pictures.map((picture, index) => {
                return <PictureCard key={uuidv4()} content={picture} last={index === pictures.length - 1} />;
              })}
            </div>
          </div>
        </div>
      ) : null}
    </MainLayout>
  );
};

const mapStateToProps = (state: ApplicationState) => ({
  pictures: state.picture.items,
});

export default connect(mapStateToProps)(HomePage);
