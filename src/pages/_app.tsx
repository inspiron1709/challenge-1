import React from 'react';
import { AppProps } from 'next/app';

import { wrapper } from '~/store';

import '@/assets/styles/index.css';

const VApp = ({ Component, pageProps }: AppProps) => {
  return <Component {...pageProps} />;
};

export default wrapper.withRedux(VApp);
